import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {MovieDetailPage} from "../movie-detail/movie-detail";
import {IMovie} from "../../interface/IMovie";
import {MovieApiProvider} from "../../providers/movie-api/movie-api";


/**
 * Generated class for the MovieListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-movie-list',
  templateUrl: 'movie-list.html',
})
export class MovieListPage {

  movies = new Array<IMovie>();
  baseurl = "https://image.tmdb.org/t/p/w185";
  searchString: string = "";


  constructor(public navCtrl: NavController, public navParams: NavParams, private movieApiProvider: MovieApiProvider, public zone: NgZone) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovieListPage');
    this.movieApiProvider.getMovies().subscribe(data => {
      this.movies = data.results;
    })
  }

  filterMovies($event) {
    this.searchString = $event.srcElement.value;
    this.zone.run(() => {
      this.movieApiProvider.searchMovie(this.searchString, false).subscribe(data => {
        this.movies = data.results;
      })
    });
  }

  getNextMoviePage($event) {
    if ($event.contentElement.localName != "ion-content") {
      return;
    }

    const scrollElement = $event.scrollElement;
    const scrollHeight = scrollElement.scrollHeight - scrollElement.clientHeight;
    const currentScrollDepth = $event.scrollTop;
    const targetPercent = 85;
    let triggerDepth = ((scrollHeight / 100) * targetPercent);

    if (currentScrollDepth > triggerDepth) {
      this.zone.run(() => {
        if (this.searchString.length > 0) {

        } else {
          this.movieApiProvider.getNextMovies().subscribe(data => {
            console.log(data.results);
            console.log("new data");
            //console.log(this.movies);
            this.movies = this.movies.concat(data.results);
          })
        }
      });
    }
  }

  goToDetail(movie: IMovie) {
    this.navCtrl.push(MovieDetailPage, {movie: movie, isFavorite: false})
  }
}
