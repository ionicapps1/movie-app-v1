import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IMovie} from "../../interface/IMovie";
import {Platform} from "ionic-angular";
import {Observable} from "rxjs/Rx";
import * as key from "./apiKey.json";

/*
  Generated class for the MovieApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieApiProvider {

  private baseUrl: string = "https://api.themoviedb.org/3/";
  private apiKey: String = key.key;
  private pages = 1;
  private searchpages = 1;

  movies: IMovie[];

  constructor(private readonly http: HttpClient, private readonly platform: Platform) {
    console.log('Hello MovieApiProvider Provider');
  }

  getMovies(): Observable<any> {
    return this.http.get(this.baseUrl + "movie/popular?api_key=ab02b2d5b97666e564bd2ece7bb1494c&language=fr&page=" + this.pages);
    //return this.http.get(`${this.baseUrl}`);
  }

  getNextMovies(): Observable<any> {
    this.pages++;
    this.searchpages = 1;
    return this.http.get(this.baseUrl + "movie/popular?api_key=" + this.apiKey + "&language=fr&page=" + this.pages);
  }

  searchMovie(search, inc): Observable<any> {
    if (inc) {
      this.searchpages++;
    }
    console.log(this.searchpages);
    return this.http.get(this.baseUrl + "search/movie?api_key=" + this.apiKey + "&language=fr&query=" + search + "&page=" + this.searchpages);
  }
}
